#!/usr/bin/env bash
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020-2021, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -n "${DEBUG:-}" ]; then
  set -x
fi

# bash strict mode
set -euo pipefail

declare -A platforms

platforms[amazonlinux:2:aarch64]=1
platforms[amazonlinux:2:x86_64]=1

platforms[centos:6:x86_64]=1
platforms[centos:7:aarch64]=1
platforms[centos:7:ppc64le]=1
platforms[centos:7:x86_64]=1
platforms[centos:8:aarch64]=1
platforms[centos:8:ppc64le]=1
platforms[centos:8:x86_64]=1

platforms[debian:10:aarch64]=1
platforms[debian:10:x86_64]=1
platforms[debian:8:x86_64]=1
platforms[debian:9:x86_64]=1

platforms[docker-auditor:4.22.0:aarch64]=1
platforms[docker-auditor:4.22.0:ppc64le]=1
platforms[docker-auditor:4.22.0:x86_64]=1

platforms[opensuse:15:aarch64]=1
platforms[opensuse:15:x86_64]=1

platforms[ubuntu:16.04:x86_64]=1
platforms[ubuntu:18.04:aarch64]=1
platforms[ubuntu:18.04:x86_64]=1
platforms[ubuntu:20.04:aarch64]=1
platforms[ubuntu:20.04:x86_64]=1

declare -A jdk_ver

jdk_ver[amazonlinux:2]="11"

     jdk_ver[centos:6]="1.8.0"
     jdk_ver[centos:7]="11"
     jdk_ver[centos:8]="11"

     jdk_ver[debian:8]="7"
     jdk_ver[debian:9]="8"
    jdk_ver[debian:10]="11"

  jdk_ver[opensuse:15]="11"

 jdk_ver[ubuntu:16.04]="9"
 jdk_ver[ubuntu:18.04]="11"
 jdk_ver[ubuntu:20.04]="14"

function get_jdk() {
  local image="$1" version="$2"

  echo "${jdk_ver["${image}:${version}"]}"
}

function supported_platform() {
  local image="$1" version="$2" arch="$3"

  [ -v platforms["${image}:${version}:${arch}"] ] \
    && return 0
}
