#!/usr/bin/env bash
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020-2021, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -n "${DEBUG:-}" ]; then
  set -x
fi

# bash strict mode
set -euo pipefail

arch="$(uname -m)"

image="${CINC_IMAGE//omnibus-/}"
context="${image:-ubuntu}"

repo="${CI_REGISTRY_IMAGE:-cincproject}"

ref="${CI_COMMIT_SHORT_SHA:-}"

function main() {

  source scripts/common.sh

  if [ -s "${context}/Dockerfile.${arch}" ] ; then
    docker_file="${context}/Dockerfile.${arch}"
  else
    docker_file="${context}/Dockerfile"
  fi

  for version in ${VERSIONS:-} ; do
    if ! supported_platform "$image" "$version" "$arch"; then
      >&2 echo 'unsupported platform'
      continue
    fi

    tag="${repo}/${CINC_IMAGE}:${version}-${arch}${ref:+-${ref}}"

    jdk="$(get_jdk "$image" "$version")"

    docker build \
      --no-cache \
      --build-arg VERSION="${version}" \
      ${jdk:+--build-arg JDK_VER="${jdk}"} \
      -t "$tag" \
      -f "$docker_file" \
      "$context"

    docker push "$tag"
  done

}

main
