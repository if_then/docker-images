#!/usr/bin/env bash

if [ -n "${DEBUG:-}" ]; then
  set -x
fi

# bash strict mode
set -euo pipefail

chef_version="${CHEF_VERSION:-14}"

install_method="${OMNIBUS_INSTALL_METHOD:-chef}"
toolchain_version="${OMNIBUS_TOOLCHAIN_VERSION:-latest}"

function main() {

  case "_${install_method}" in
    _package)
      ;;
    _chef)
      wget -qO- "https://www.chef.io/chef/install.sh" \
        | bash -s -- -v "$chef_version"

      mkdir -p /var/chef/{cache,cookbooks}

      wget -qO- "https://supermarket.chef.io/cookbooks/omnibus/download" \
        | tar -xzC /var/chef/cookbooks

      for dep in \
        build-essential \
        chef-ingredient \
        chef-sugar \
        git \
        homebrew \
        mingw \
        remote_install \
        seven_zip \
        windows \
        wix \
      ; do \
        wget -qO- "https://supermarket.chef.io/cookbooks/${dep}/download" \
          | tar -xzC /var/chef/cookbooks
      done

      echo "{
        \"omnibus\":{
          \"toolchain_version\":\"${toolchain_version}\"
        }
      }" > /tmp/attr.json

      chef-solo -o 'recipe[omnibus]' -j /tmp/attr.json

      rm -rf /tmp/attr.json
      ;;
  esac

  sed -i -e 's/^env.*//' /home/omnibus/load-omnibus-toolchain.sh

}

main
